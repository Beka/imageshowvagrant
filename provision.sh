# First we update all the packages in this VM
echo "$(tput setaf 6)Updating the packages ..."
sudo apt-get update > /dev/null
#
tput setaf 2; echo "**********"
tput setaf 2; echo "** DONE **"
tput setaf 2; echo "**********"
#
# Install Nginx server and GIT
echo "$(tput setaf 6)Installing Nginx server and GIT ..."
sudo apt-get -y install nginx > /dev/null
sudo apt-get -y install git > /dev/null
#
# Restarting the nginx server
echo "$(tput setaf 6)Restarting nginx server ..."
sudo service nginx restart
#
echo "$(tput setaf 2)**********"
echo "$(tput setaf 2)** DONE **"
echo "$(tput setaf 2)**********"
#
# Install nodeJS and NPM
echo "$(tput setaf 6)Installing NodeJS and NPM ..."
sudo apt-get -y install nodejs npm > /dev/null
sudo ln -s /usr/bin/nodejs /usr/bin/node
#
echo "$(tput setaf 2)**********"
echo "$(tput setaf 2)** DONE **"
echo "$(tput setaf 2)**********"
#
# Install Bower and Gulp globally
echo "$(tput setaf 6)Installing Bower, PhantomJS, Karma and Gulp globally ..."
sudo npm install -g bower
sudo npm install -g gulp
sudo npm install -g phantomjs
sudo npm install -g karma-cli
#
echo "$(tput setaf 2)**********"
echo "$(tput setaf 2)** DONE **"
echo "$(tput setaf 2)**********"
#
# END NOTICE
echo "$(tput setaf 1)INSTALLATION COMPLETE!"
echo "$(tput setaf 1)You can go to /usr/share/nginx/html/ to clone the repo"
echo "$(tput setaf 7)Enjoy ..."
